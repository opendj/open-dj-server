
exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTable('room', t => {
      t.increments('id').unsigned().primary()
      t.dateTime('createdAt').notNull()
      t.dateTime('updatedAt').nullable()
      t.string('name').notNull()
      t.string('password').notNull()
      t.enum('status', ['TAKEN', 'AVAILABLE']).notNull()

      t.timestamps(true, true)
    }),
    knex.schema.createTable('user', t => {
      t.increments('id').unsigned().primary()
      t.string('socketId').notNull()
      t.string('nickname').nullable()

      t.timestamps(true, true)
    }),
    knex.schema.createTable('track', t => {
      t.increments('id').unsigned().primary()
      t.string('spotifyId').notNull()

      t.timestamps(true, true)
    }),
    knex.schema.createTable('candidate', t => {
      t.increments('id').unsigned().primary()
      t.integer('trackId').unsigned()
      t.integer('userId').unsigned()
      t.integer('roomId').unsigned()
      t.enum('role', ['QUEUE', 'BATTLE', 'WINNER', 'LOSER']).notNull()
      t.foreign('trackId').references('track.id')
      t.foreign('userId').references('user.id')
      t.foreign('roomId').references('room.id')

      t.timestamps(true, true)
    }),
    knex.schema.createTable('vote', t => {
      t.integer('candidateId').unsigned()
      t.integer('userId').unsigned()
      t.foreign('candidateId').references('candidate.id')
      t.foreign('userID').references('user.id')

      t.timestamps(true, true)
    }),
  ])
}

exports.down = function(knex, Promise) {
  
}
