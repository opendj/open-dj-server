FROM node:8.4.0

RUN mkdir /src
COPY . /src
RUN cd /src
RUN npm install

CMD npm run build