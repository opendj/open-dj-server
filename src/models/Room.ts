export default interface Room {
  id?: number,
  name: string,
  password: string,
  status: string,
  refreshToken: string,
  playlistId: string
}
