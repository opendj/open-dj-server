export default interface User {
  id?: number,
  socketId: string,
  roomId: number
  name?: string,
}
