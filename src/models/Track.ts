import Album from './Album'

export default interface Track {
  id: number,
  spotifyId: string,
  name: string,
  album: Album
}
