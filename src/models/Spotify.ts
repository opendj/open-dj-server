export interface SpotifyAuthResponse {
  accessToken: string,
  refreshToken: string
}

export interface SpotifyUserResponse {
  id: string
}

export interface SpotifyPlaylistResponse {
  id: string
}
