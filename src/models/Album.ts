import Artist from './Artist'

export default interface Album {
  id?: string,
  name: string,
  artists: Array<Artist>
}
