import TrackDao from '../dao/TrackDao'
import Track from '../models/Track'
import Room from '../models/Room'
import User from '../models/User'

interface Parameters {
  trackDao: TrackDao
}

export default class TrackService {
  private trackDao: TrackDao

  constructor (args: Parameters) {
    this.trackDao = args.trackDao
  }

  async add (spotifyId: string, room: Room): Promise<Track> {
    // TODO: check whether this track currently
    // exists in the local database

    // TODO: If not, use Spotify API to check whether the track exists

    // TODO: If not, add the track to the local database

    // TODO: add a candidate with role BATTLE or QUEUE depending
    // on the current battle

    throw new Error('Method not implemented')
  }

  async vote (candidateId: number, user: User): Promise<void> {
    // TODO: Fetch the candidate + track

    // TODO: Compare the candidate room to the user room
    // and throw exception if not the same

    // TODO: Remove votes for other current battling candidates

    // TODO: Create votes for the current candidate
    throw new Error('Method not implemented')
  }

  async getVotes (trackId: number, roomId: number): Promise<User> {
    // TODO: Fetch all intermediate rows
    // TODO: Fetch all users
    // TODO: Return users
    throw new Error('Method not implemented')
  }
}
