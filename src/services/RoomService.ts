import SpotifyService from './SpotifyService'
import RoomDao from '../dao/RoomDao'
import Room from '../models/Room'
import User from '../models/User'

interface Parameters {
  roomDao: RoomDao,
  spotifyService: SpotifyService
}

export default class RoomService {
  private roomDao: RoomDao
  private spotify: SpotifyService

  constructor (args: Parameters) {
    this.roomDao = args.roomDao
    this.spotify = args.spotifyService
  }

  async create (name: string, spotifyCode: string): Promise<Room> {
    // Find any existing room with the same name
    let existingRoom = await this.findByRoomName(name)

    // If a room is found, check whether it is taken
    if (existingRoom && existingRoom.status === 'TAKEN') {
      // If taken, throw new Error()
      throw new Error(`Room "${name}" is currently not available`)
    }

    const password = (Math.round(Math.random() * 10000) + 1000).toString()
    const credentials = await this.spotify.authenticateWithCode(spotifyCode)
    this.spotify.setAccessToken(credentials.accessToken)

    // Get the current user profile. Its ID must be used
    // to create a playlist
    const currentUser = await this.spotify.getUserProfile()

    // And we need to create a new playlist for the current session
    const playlist = await this.spotify.createPlaylist(currentUser.id, `OpenDJ - ${name}`)
    const room = {
      name,
      password,
      status: 'TAKEN',
      refreshToken: credentials.refreshToken,
      playlistId: playlist.id
    }

    return this.roomDao.create(room)
  }

  async findByRoomName (name: string): Promise<Room> {
    return this.roomDao.findByName(name)
  }

  async findForUser (user: User): Promise<Room> {
    return this.roomDao.find(user.roomId)
  }

  async authorize (roomName: string, password: string): Promise<Boolean> {
    const room = await this.findByRoomName(roomName)

    return room.password === password
  }
}
