import {
  SpotifyAuthResponse,
  SpotifyUserResponse,
  SpotifyPlaylistResponse
} from '../models/Spotify'

export default interface SpotifyService {
  setAccessToken (accessToken: string): void

  authenticateWithCode (spotifyCode: string): Promise<SpotifyAuthResponse>

  authenticateWithRefreshToken (refreshToken: string): Promise<SpotifyAuthResponse>

  getUserProfile (): Promise<SpotifyUserResponse>

  createPlaylist (userId: string, name: string): Promise<SpotifyPlaylistResponse>
}
