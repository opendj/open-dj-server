import SpotifyService from './SpotifyService'
import axios from 'axios'
import {
  SpotifyAuthResponse,
  SpotifyUserResponse,
  SpotifyPlaylistResponse
} from '../models/Spotify'

export default class SpotifyServiceImpl implements SpotifyService {
  private apiKey: string
  private apiSecret: string
  private redirectUri: string
  private accessToken: string

  constructor (apiKey: string, apiSecret: string, redirectUri: string, accessToken: string) {
    this.apiKey = apiKey
    this.apiSecret = apiSecret
    this.redirectUri = redirectUri
    this.accessToken = accessToken
  }

  setAccessToken (accessToken: string) {
    this.accessToken = accessToken
  }

  async authenticateWithCode (spotifyCode: string): Promise<SpotifyAuthResponse> {
    return axios.post('/api/token', {
      code: spotifyCode,
      redirect_uri: this.redirectUri,
      client_id: this.apiKey,
      client_secret: this.apiSecret,
      grant_type: 'code'
    }).then(response => response.data)
  }

  authenticateWithRefreshToken (refreshToken: string): Promise<SpotifyAuthResponse> {
    throw new Error('Method not implemented.')
  }

  async getUserProfile (): Promise<SpotifyUserResponse> {
    return axios.get('/v1/me', {
      headers: { Authorization: `Bearer ${this.accessToken}` }
    }).then(response => response.data)
  }

  async createPlaylist (userId: string, name: string): Promise<SpotifyPlaylistResponse> {
    return axios.post(`/v1/users/${userId}/playlists`, { name }, {
      headers: { Authorization: `Bearer ${this.accessToken}`}
    }).then(response => response.data)
  }
}
