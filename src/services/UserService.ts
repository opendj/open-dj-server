import UserDao from '../dao/UserDao'
import User from '../models/User'

interface Parameters {
  userDao: UserDao
}

export default class UserService {
  private userDao: UserDao

  constructor (args: Parameters) {
    this.userDao = args.userDao
  }

  async create (socketId: string, roomId: number): Promise<User> {
    const user = { socketId, roomId }
    return this.userDao.create(user)
  }

  async updateProfile (socketId: string, name: string): Promise<User> {
    const user = await this.userDao.findBySocketId(socketId)
    user.name = name

    return this.userDao.updateProfile(user.id, user)
  }

  async findBySocketId (socketId: string): Promise<User> {
    return this.userDao.findBySocketId(socketId)
  }
}
