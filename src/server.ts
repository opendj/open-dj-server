import * as Io from 'socket.io'
import * as awilix from 'awilix'
import RoomService from './services/RoomService'
import UserService from './services/UserService'
import TrackService from './services/TrackService'

interface EventRoomCreate {
  name: string,
  spotifyCode: string
}

interface EventRoomJoin {
  name: string,
  password: string
}

interface EventVote {
  candidateId: number
}

interface EventTrackAdd {
  spotifyId: string
}

type Ack = (...args: any[]) => void

class Application {
  private container: awilix.AwilixContainer

  constructor (container: awilix.AwilixContainer) {
    this.container = container
  }

  run (port: number) {
    const io = Io()

    const roomService: RoomService = this.container.resolve('roomService')
    const userService: UserService = this.container.resolve('userService')
    const trackService: TrackService = this.container.resolve('trackService')

    const nspDashboard = io.of('/dashboard')
    const nspClient = io.of('/client')

    nspDashboard.on('connection', socket => {
      socket.on('room:create', async (data: EventRoomCreate, ack: Ack) => {
        try {
          const room = await roomService.create(data.name, data.spotifyCode)
          ack(null, room)
        } catch (err) {
          ack(`Room "${data.name}" could not be created.`)
        }
      })
    })

    nspClient.on('connection', socket => {
      socket.on('room:join', async (data: EventRoomJoin, ack: Ack) => {
        try {
          const room = await roomService.findByRoomName(data.name)
          const isAuthorized = await roomService.authorize(room.name, data.password)

          if (isAuthorized) {
            const user = await userService.create(socket.id, room.id)
            // TODO: use the room to create a DI container for this socket
          } else {
            throw new Error(`Client "${socket.id}" attempted to join room "${data.name}"`)
          }
        } catch (err) {
          ack(`Could not join room "${data.name}"`)
        }
      })

      socket.on('vote', async (data: EventVote, ack: Ack) => {
        try {
          const user = await userService.findBySocketId(socket.id)

          await trackService.vote(data.candidateId, user)

          ack(null, { success: true })
          nspClient.emit('battle:update' /* TODO: Fetch current room battle*/)
          nspDashboard.emit('battle:update' /* TODO: Fetch current room battle*/)
        } catch (err) {
          ack(`Could not vote for track "${data.candidateId}"`, { success: false })
        }
      })

      socket.on('track:add', async (data: EventTrackAdd, ack: Ack) => {
        try {
          const user = await userService.findBySocketId(socket.id)
          const room = await roomService.findForUser(user)
          await trackService.add(data.spotifyId, room)

          nspDashboard.emit('battle:update' /* TODO: Fetch current room battle */)
          nspDashboard.emit('queue:update' /* TODO: Fetch current room queue */)
        } catch (err) {
          ack(`Could not add track ${data.spotifyId}`)
        }
      })
    })

    io.listen(port)
  }
}

export default Application
