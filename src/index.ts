import * as awilix from 'awilix'
import Server from './server'
import RoomDaoImpl from './dao/RoomDaoImpl'
import UserDaoImpl from './dao/UserDaoImpl'
import TrackDaoImpl from './dao/TrackDaoImpl'
import RoomService from './services/RoomService'
import UserService from './services/UserService'
import TrackService from './services/TrackService'
import SpotifyService from './services/SpotifyServiceImpl'
import Room from './models/Room'
import knex from './db'

const container = awilix.createContainer()

container.register({
  knex: awilix.asValue(knex),
  roomDao: awilix.asClass(RoomDaoImpl),
  userDao: awilix.asClass(UserDaoImpl),
  trackDao: awilix.asClass(TrackDaoImpl),
  roomService: awilix.asClass(RoomService),
  userService: awilix.asClass(UserService),
  trackService: awilix.asClass(TrackService),
  spotifyService: awilix.asClass(SpotifyService)
})

const server = new Server(container)

server.run(Number.parseInt(process.env.PORT) || 3000)
