import User from '../models/User'

export default interface UserDao {
  create (user: User): Promise<User>

  findBySocketId (socketId: string): Promise<User>

  updateProfile (userId: number, user: User): Promise<User>
}
