import * as Knex from 'knex'
import TrackDao from './TrackDao'
import Room from '../models/Room'
import Track from '../models/Track'

interface Parameters {
  knex: Knex
}

export default class TrackDaoImpl implements TrackDao {
  private static tableName = 'track'
  private knex: Knex

  constructor (args: Parameters) {
    this.knex = args.knex
  }

  async create (spotifyId: string): Promise<Track> {
    return this.knex(TrackDaoImpl.tableName).insert({ spotifyId })
  }

  async findBySpotifyId (spotifyId: string): Promise<Track> {
    const [track] = await this.knex(TrackDaoImpl.tableName).where({ spotifyId })
    // TODO: fetch track information from the spotify api
    return track
  }

  async findBattle (room: Room): Promise<Track[]> {
    // TODO: fetch each track information from the spotify api
    
    return this.knex.select('track.*')
      .from('track')
      .innerJoin('candidate', 'track.id', 'candidate.trackId')
      .where({
        'candidate.roomId': room.id,
        'candidate.role': 'BATTLE'
      })
  }

  async findQueue (room: Room): Promise<Track[]> {
    // TODO: fetch each track information from the spotify api

    return this.knex.select('track.*')
    .from('track')
    .innerJoin('candidate', 'track.id', 'candidate.trackId')
    .where({
      'candidate.roomId': room.id,
      'candidate.role': 'QUEUE'
    })
  }
}
