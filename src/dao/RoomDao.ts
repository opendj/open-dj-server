import Room from '../models/Room'

export default interface RoomDao {
  create (room: Room): Promise<Room>

  find (roomId: number): Promise<Room>

  findByName (name: string): Promise<Room>

  findForUser (userId: number): Promise<Room>
}
