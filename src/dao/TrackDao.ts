import Room from '../models/Room'
import Track from '../models/Track'

export default interface TrackDao {
  create (spotifyId: string): Promise<Track>

  findBySpotifyId (spotifyId: string): Promise<Track>

  findBattle (room: Room): Promise<Array<Track>>

  findQueue (room: Room): Promise<Array<Track>>
}
