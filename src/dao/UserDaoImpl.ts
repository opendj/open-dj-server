import * as Knex from 'knex'
import UserDao from './UserDao'
import User from '../models/User'

interface Parameters {
  knex: Knex
}

export default class UserDaoImpl implements UserDao {
  private static tableName = 'user'
  private knex: Knex

  constructor (args: Parameters) {
    this.knex = args.knex
  }

  async create (user: User): Promise<User> {
    return this.knex(UserDaoImpl.tableName).insert({ user })
  }

  async findBySocketId (socketId: string): Promise<User> {
    const [user] = await this.knex(UserDaoImpl.tableName).where({ socketId })
    return user
  }

  async updateProfile (userId: number, user: User): Promise<User> {
    return this.knex(UserDaoImpl.tableName)
      .where('id', userId)
      .update(Object.assign({}, user, {
        updated_at: new Date()
      }))
  }
}
