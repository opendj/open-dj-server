import * as Knex from 'knex'
import RoomDao from './RoomDao'
import Room from '../models/Room'

interface Parameters {
  knex: Knex
}

class RoomDaoImpl implements RoomDao {
  private static tableName = 'room'
  private static statusTaken = 'TAKEN'
  private static statusAvailable = 'AVAILABLE'
  private knex: Knex

  constructor (args: Parameters) {
    this.knex = args.knex
  }

  async create (room: Room): Promise<Room> {
    return this.knex(RoomDaoImpl.tableName).insert(room)
  }

  async find (roomId: number): Promise<Room> {
    const [room] = await this.knex(RoomDaoImpl.tableName).where({ id: roomId })

    return room
  }

  async findByName (name: string): Promise<Room> {
    const [room] = await this.knex(RoomDaoImpl.tableName).where({
      name,
      status: RoomDaoImpl.statusTaken
    })

    return room
  }

  async findForUser (userId: number): Promise<Room> {
    const [room] = await this.knex.select('room.*')
      .from('room')
      .innerJoin('user', 'room.id', 'user.roomId')
      .where({ 'user.id': userId })

    return room
  }
}

export default RoomDaoImpl
