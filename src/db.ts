import * as Knex from 'knex'
import * as config from '../knexfile'

const knex = Knex(config)
knex.migrate.latest()

export default knex
